import java.util.ArrayList;

import com.devcamp.j53_basicjava.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        ArrayList<Person> listPeople = new ArrayList<Person>();
        Person person1 = new Person();
        

        Person person2 = new Person("Tran My Linh", 28, 53);
        
        Person person3 = new Person("Ho Ngoc Ha", 26, 78, 15000000);
       
        Person person4 = new Person("Ho Hoai Anh", 26, 78, new String[] {"dog", "cat"});
        
        Person person5 = new Person("Hong Dang", 26, 78, 17000000, new String[] {"dog", "cat"});

        listPeople.add(person1);
        listPeople.add(person2);
        listPeople.add(person3);
        listPeople.add(person4);
        listPeople.add(person5);
        
        for (Person person: listPeople) {
            System.out.println(person);
        }
    }
}
